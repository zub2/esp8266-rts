# esp8266-rts

Firmware for ESP8266 that can send Somfy RTS frames using a [RFM69](https://www.hoperf.com/%20modules/rf_transceiver/RFM69HW.html) RF module.

The protocol is used by some blinds, shades, rolling shutters, awnings, etc. by [Somfy](https://www.somfysystems.com/).

The code is based on the description available at [Pushstack blog](https://pushstack.wordpress.com/somfy-rts-protocol/).

## Hardware

This software runs on ESP8266. It's been tested with ESP-07. It might require some tweaking if you want to run it on a different ESP8266 board. It requires SPI, 2 GPIO inputs that can trigger interrupts and 1 GPIO output, so not all ESP8266 boards can be used. (Though it would be possible to avoid the GPIOs completely and just use SPI.)

For details see the [KiCad](http://www.kicad.org/) schematic in directory [hw](hw/README.md).

## Compiling and flashing

You will need [ESP8266_RTOS_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) > v3.2 to build this project. Either use Make or (if it happens to work with your version of the SDK), you can use CMake.

Unfortunately the `v3.2` tag is not sufficient because in it the `esp_console` does not compile. It is fixed in later commits in the `release/v3.2` branch. At this time e.g. the commit `efd0a545ac` works.

Standard build instructions for the ESP8266_RTOS_SDK apply: You need to set the environment variable `IDF_PATH` to the root of the SDK and you need to make sure the xtensa gcc toolchain is in your `PATH`.

There are some options accessible via `make menuconfig` - in `esp8266-rts Configuration`:

* RFM module type ("H" series, or non-"H" series). *If this is not configured correctly, no transmissions are made.*
* Default Wi-Fi settings (SSID, password, hostname template and TCP port number). This can be changed at runtime via serial console.
* Enable radio debugging Wi-Fi commands. *These commands allow complete remote access to the RFM module.*

## Configuring

Apart from configuration of default values at compile time, it is also possible to change the configuration using serial console. Typing in `help` followed by enter lists the available commands.

## Security

Please note that *the protocol is not secure*: There is no authentication and no crypto. So anyone who is able to sent TCP packets on the Wi-Fi network to which the ESP is connected can send any RTS packet. And anyone able to intercept TCP packets on the Wi-Fi can see all the requests sent to the ESP.

If the software is built with `RADIO_DEBUG_WIFI_COMMANDS` enabled, then it's possible to completely control the RFM module remotely. So it's possible to broadcast and also receive other things than just RTS frames.

## Usage

Once the ESP module is running and configured, it connects to Wi-Fi and listens for TCP connections (the default port number is 9772). The protocol is very simple and it is not secure.

There is a stand-alone python client that can be used for testing in `tools/esp8266-rts-client.py`. This software can be used together with [rts-tools](https://gitlab.com/zub2/rts-tools) and [rts-controller](https://gitlab.com/zub2/rts-controller).

## Useful Links

* [Somfy RTS protocol description](https://pushstack.wordpress.com/somfy-rts-protocol/)
* [rts-tools](https://gitlab.com/zub2/rts-tools)
* [rts-controller](https://gitlab.com/zub2/rts-controller)
