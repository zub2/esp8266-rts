# esp8266-rts Hardware

This directory contains a [KiCAD](https://kicad-pcb.org/) project (schematic and PCB) of a circuit that can run the software in this repository.

_Disclaimer: I'm a hardware noob. While the circuit works OK for me, do your own research. Any ideas for improvement are welcome._

The circuit is quite simple: Just an ESP-07 module, an RFM69HW module, and the necessary trifles (resistors, capacitors, linear voltage regulator, connectors) to make it work.

The components (and so the PCB footprints) were adapted to whatever was easily available to me, but it should be quite easy to adapt to whatever you can source - e.g. different regulator or different connectors. I adapted the footprints of some components to make the pads a bit larger - for easier soldering for noobs. :) The most challenging part to solder is the micro USB connector.

With some soldering (adding wires to the ESP and RFM modules), it's possible to assemble this even on a breadboard without any PCB:

![circuit on a breadboard](images/esp8266-rts-hw-breadboard.jpg)

But it seems that it's no problem having the PCBs made in a PCB shop:

![circuit on a breadboard](images/esp8266-rts-hw-pcb.jpg)
