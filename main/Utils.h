/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_UTILS_H
#define ESP8266_RTS_UTILS_H

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

#include <cstring>
#include <algorithm>

extern "C"
{
	// this needs to be defined in the global namespace, and ideally extern "C"
	// because portEND_SWITCHING_ISR is a macro that declares vTaskSwitchContext
	// which is extern "C" and is in the global namespace. Invoking the macro
	// from a different namespace causes link error.
	inline void yieldFromISR(BaseType_t xSwitchRequired)
	{
	#ifdef portYIELD_FROM_ISR
		portYIELD_FROM_ISR(xSwitchRequired);
	#else
		portEND_SWITCHING_ISR(xSwitchRequired);
	#endif
	}
}

// no std::size in the SDK
template<typename T, size_t N>
constexpr size_t size(const T (&array)[N]) noexcept
{
	return N;
}

inline constexpr uint8_t BIT_u8(uint8_t i)
{
	return uint8_t(1u << i);
}

inline constexpr uint32_t BIT_u32(uint8_t i)
{
	return uint32_t(1u << i);
}

[[ noreturn ]] void fatalError(const char * msg);

template<size_t N>
void copyToCharArray(char(&dest)[N], const char * src)
{
	if (N > 0)
	{
		strncpy(dest, src, N - 1);
		dest[N-1] = 0;
	}
}

template<size_t N, size_t M>
void convertUint8ToCharArray(char(&dest)[N], const uint8_t(&src)[M])
{
	if (N > 0)
	{
		const size_t count = std::min(M, N - 1);
		if (count > 0)
			strncpy(dest, reinterpret_cast<const char*>(src), count);
		dest[N-1] = 0;
	}
}

template<typename T, size_t N>
void copyArray(T(&dest)[N], const T(&src)[N])
{
	std::copy(src, std::end(src), dest);
}

class MutexGuard
{
public:
	explicit MutexGuard(SemaphoreHandle_t mutex):
		m_mutex(mutex)
	{
		xSemaphoreTake(m_mutex, portMAX_DELAY);
	}

	~MutexGuard()
	{
		xSemaphoreGive(m_mutex);
	}

private:
	SemaphoreHandle_t m_mutex;
};

#endif // ESP8266_RTS_UTILS_H
