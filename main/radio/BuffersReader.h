/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_BUFFERS_READER_H
#define ESP8266_RTS_BUFFERS_READER_H

#include <cstdint>
#include <algorithm>

class BuffersReader
{
public:
	BuffersReader(const uint8_t * const * dataBuffers, const size_t * sizes, size_t bufferCount):
		m_dataBuffers(dataBuffers),
		m_sizes(sizes),
		m_bufferCount(bufferCount),
		m_bufferIndex(0)
	{
		if (m_bufferIndex < bufferCount)
		{
			m_bufferPointer = m_dataBuffers[m_bufferIndex];
			nextBufferIfAtBufferEnd();
		}
	}

	// return at block of at least maxSize bytes, but less if not enough bytes
	// are available in the current buffer or all buffers were processed already
	void getChunk(uint8_t const* & chunk, size_t & chunkSize)
	{
		if (isAtEnd())
		{
			chunk = nullptr;
			chunkSize = 0;
		}
		else
		{
			chunkSize = std::min(chunkSize, m_sizes[m_bufferIndex] - (m_bufferPointer - m_dataBuffers[m_bufferIndex]));
			chunk = m_bufferPointer;
			m_bufferPointer += chunkSize;
			nextBufferIfAtBufferEnd();
		}
	}

	bool isAtEnd() const
	{
		return m_bufferIndex >= m_bufferCount;
	}

private:
	void nextBufferIfAtBufferEnd()
	{
		while (!isAtEnd() && m_bufferPointer == m_dataBuffers[m_bufferIndex] + m_sizes[m_bufferIndex])
		{
			m_bufferIndex++;
			m_bufferPointer = m_dataBuffers[m_bufferIndex];
		}
	}

	uint8_t const * const * m_dataBuffers;
	size_t const * m_sizes;
	size_t m_bufferCount;
	size_t m_bufferIndex;
	uint8_t const * m_bufferPointer;
};

#endif // ESP8266_RTS_BUFFERS_READER_H
