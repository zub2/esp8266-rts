/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_MANCHESTER_ENCODER_H
#define ESP8266_RTS_MANCHESTER_ENCODER_H

#include <cstdint>

template<typename TBitStream>
class ManchesterEncoder
{
public:
	ManchesterEncoder(TBitStream & bs):
		m_bs(bs)
	{}

	ManchesterEncoder & operator<<(uint8_t b)
	{
		uint8_t mask = 1 << 7;
		for (unsigned i = 0; i < 8; i++)
		{
			const bool bit = (b & mask) != 0;
			appendBit(bit);
			mask >>= 1;
		}
		return *this;
	}

private:
	void appendBit(bool b)
	{
		m_bs << !b << b;
	}

	TBitStream & m_bs;
};

#endif // ESP8266_RTS_MANCHESTER_ENCODER_H
