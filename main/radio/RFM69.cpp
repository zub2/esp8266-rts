/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RFM69.h"

#include "freertos/task.h"

#include "rom/ets_sys.h"

#include "driver/gpio.h"
#include "driver/spi.h"

#include "../Utils.h"

namespace
{
	spi_interface_t makeSPIInterfaceCfg()
	{
		spi_interface_t i;

		i.cpol = 0;
		i.cpha = 0;
		i.bit_tx_order = 0;
		i.bit_rx_order = 0;

		// big endian
		i.byte_tx_order = 0;
		i.byte_rx_order = 0;

		i.mosi_en = 1;
		i.miso_en = 1;
		i.cs_en = 1;

		return i;
	}

#if CONFIG_RFM69_MODEL_H
	void setHighPowerMode(bool enable)
	{
		rfm69::writeRegister(rfm69::RegTestPa1, enable ? 0x5D : 0x55);
		rfm69::writeRegister(rfm69::RegTestPa2, enable ? 0x7C : 0x70);
	}
#endif
}

namespace rfm69
{
	void reset()
	{
		// RFM69HW spec, 7.2.2. Manual Reset

		taskENTER_CRITICAL();

		// Set RESET_GPIO to H for ≥100µs, then set it back to L (which
		// gets turned to high-Z).
		gpio_set_level(RESET_GPIO, 1);
		os_delay_us(100);
		gpio_set_level(RESET_GPIO, 0);

		taskEXIT_CRITICAL();

		// wait for at least 5ms
		vTaskDelay((5 + portTICK_PERIOD_MS - 1) / portTICK_PERIOD_MS);
	}

	bool setTxPower(int level)
	{
		/*
		* This is based on the excellent work by André Hessling
		* https://andrehessling.de/2015/02/07/figuring-out-the-power-level-settings-of-hoperfs-rfm69-hwhcw-modules/
		* and https://github.com/ahessling/RFM69-STM32
		*/
#if CONFIG_RFM69_MODEL_H
		constexpr uint8_t Pa1On = 0b01000000;
		constexpr uint8_t Pa2On = 0b00100000;

		// PA0 is not available, but PA1 and PA2 are
		if (level < -2 || level > 20)
		{
			ESP_LOGE("RFM69", "%s: requested power level %d dBm which is out of range for H-variant (-2..20 dBm)", __func__, level);
			return false;
		}

		if (level <= 13)
		{
			// disable high-power setting
			setHighPowerMode(false);

			// use PA1 only
			writeRegister(RegPaLevel, Pa1On | (level + 18));
		}
		else if (level <= 17)
		{
			// disable high-power setting
			setHighPowerMode(false);

			// use PA1 + PA2
			writeRegister(RegPaLevel, Pa1On | Pa2On | (level + 14));
		}
		else
		{
			// enable high-power setting
			setHighPowerMode(true);

			// use PA1 + PA2
			writeRegister(RegPaLevel, Pa1On | Pa2On | (level + 11));
		}
#else
		constexpr uint8_t Pa0On = 0b10000000;

		// only PA0 is available
		if (level < -18 || level > 13)
		{
			ESP_LOGE("RFM69", "%s: requested power level %d dBm which is out of range for non-H variant (-18..+13 dBm)", __func__, level);
			return false;
		}

		// use PA0
		writeRegister(RegPaLevel, Pa0On | (level + 18));
#endif

		return true;
	}

	void initGPIO()
	{
		/*
		 * Set RESET_GPIO to L - this is turned to high-Z by the circuitry
		 * between ESP8266 and RFM69.
		 */
		gpio_set_level(RESET_GPIO, 0);
		gpio_config_t c;
		c.pin_bit_mask = BIT_u32(RESET_GPIO);
		c.mode = GPIO_MODE_OUTPUT;
		c.pull_up_en = GPIO_PULLUP_DISABLE;
		c.pull_down_en = GPIO_PULLDOWN_DISABLE;
		c.intr_type = GPIO_INTR_DISABLE;
		gpio_config(&c);

		// configure GPIOs connected to RFM69 DIOx pins as inputs
		// don't enable interrupts yet
		c.pin_bit_mask = BIT_u32(DIO0_GPIO) | BIT_u32(DIO1_GPIO);
		c.mode = GPIO_MODE_INPUT;
		gpio_config(&c);
	}

	void initSPI()
	{
		spi_config_t spiConfig;

		spiConfig.interface = makeSPIInterfaceCfg();
		spiConfig.intr_enable.val = SPI_MASTER_DEFAULT_INTR_ENABLE; // ???
		spiConfig.mode = SPI_MASTER_MODE;
		spiConfig.clk_div = SPI_2MHz_DIV;
		spiConfig.event_cb = nullptr;

		spi_init(SPI_BUS, &spiConfig);
	}
}
