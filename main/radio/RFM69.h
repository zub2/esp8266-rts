/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_RFM69_H
#define ESP8266_RTS_RFM69_H

#include "esp_log.h"
#include "driver/spi.h"
#include "driver/gpio.h"

#include "../Utils.h"

namespace rfm69
{
	/*
	 * GPIO connected to RFM69's RESET pin.
	 * It must be high-Z normally and H for reset. GPIO 3 and 4 can do this
	 * directly (https://www.esp8266.com/viewtopic.php?f=32&t=16141).
	 *
	 * But because I wanted 2 GPIOs for input interrupts (and I wanted to
	 * keep the GPIO2 which is connected to LED for LED blinking), I use
	 * GPIO 16. But it needs some transistor fiddling to turn L into high-Z
	 * as it seems it can't do high-Z at all.
	 * (https://tttapa.github.io/ESP8266/Chap04%20-%20Microcontroller.html)
	 */
	constexpr gpio_num_t RESET_GPIO = GPIO_NUM_16;

	// These must be other than GPIO16 as GPIO16 can't trigger an interrupt.
	constexpr gpio_num_t DIO0_GPIO = GPIO_NUM_4;
	constexpr gpio_num_t DIO1_GPIO = GPIO_NUM_5;

	// SPI bus to use - pretty much has to be the HSPI
	constexpr spi_host_t SPI_BUS = HSPI_HOST;

	// This is a restriction of the ESP8266_RTOS_SDK's spi "driver".
	constexpr unsigned MAX_WRITE_REGS_BYTES = 64;

	// RFM69 registers that we need to access
	constexpr uint8_t RegFifo = 0x00;
	constexpr uint8_t RegOpMode = 0x01;
	constexpr uint8_t RegDataModul = 0x02;
	constexpr uint8_t RegBitrateMsb = 0x03;
	constexpr uint8_t RegFrfMsb = 0x07;
	constexpr uint8_t RegVersion = 0x10;
	constexpr uint8_t RegPaLevel = 0x11;
	constexpr uint8_t RegDioMapping1 = 0x25;
	constexpr uint8_t RegDioMapping2 = 0x26;
	constexpr uint8_t RegIrqFlags1 = 0x27;
	constexpr uint8_t RegIrqFlags2 = 0x28;
	constexpr uint8_t RegPreambleMsb = 0x2c;
	constexpr uint8_t RegSyncConfig = 0x2e;
	constexpr uint8_t RegPacketConfig1 = 0x37;
	constexpr uint8_t RegPayloadLength = 0x38;
	constexpr uint8_t RegFifoThresh = 0x3c;
	constexpr uint8_t RegTestPa1 = 0x5a;
	constexpr uint8_t RegTestPa2 = 0x5c;

	// bit ModeReady in ReqIrqFlags1
	constexpr uint8_t IRQ_FLAGS1_BIT_MODE_READY = 7;

	/**
	 * Constants from the RFM69 datasheet.
	 *
	 * See e.g. https://www.hoperf.com/data/upload/portal/20190306/RFM69HW-V1.3%20Datasheet.pdf
	 */
	constexpr size_t FIFO_SIZE = 66;
	constexpr double FXOSC = 32e6; // 32 MHz
	constexpr double FSTEP = static_cast<double>(FXOSC)/(2<<18);

#if CONFIG_RFM69_MODEL_H
	constexpr int MIN_TX_POWER = -2;
	constexpr int MAX_TX_POWER = +20;
#else
	constexpr int MIN_TX_POWER = -18;
	constexpr int MAX_TX_POWER = +13;
#endif

	// configurable options
	constexpr size_t FIFO_THRESHOLD = FIFO_SIZE / 2;

	void reset();

	inline void writeRegister(uint8_t r, uint8_t value)
	{
		alignas(uint32_t) uint8_t data[] =
		{
			uint8_t(r | 0b10000000),
			value
		};

		spi_trans_t t;
		t.mosi = reinterpret_cast<uint32_t*>(&data);
		t.bits.val = 0;
		t.bits.mosi = 8 * sizeof(data);

		spi_trans(SPI_BUS, &t);
	}

	// count can't be more than 64 bytes
	inline void writeRegisters(uint8_t r, const uint8_t * values, size_t count)
	{
		if (count > MAX_WRITE_REGS_BYTES)
		{
			ESP_LOGE("RFM69", "%s: asked to write %u bytes, but the maximum is %u", __func__, count, MAX_WRITE_REGS_BYTES);
			return;
		}

		spi_trans_t t;
		t.bits.val = 0;

		uint16_t cmd = r | 0b10000000;
		t.cmd = &cmd;
		t.bits.cmd = 8;

		/*
		 * The SPI "driver" requires the data to be 32-bit aligned. This sucks.
		 * But there is a work-around: The address field is not needed here,
		 * yet it can store 0 to 32 bits which get sent before the data. So the
		 * data pointer can be rounded up to a multiple of 4 (32 bits) and the
		 * skipped data can be loaded into addr.
		 */
		uint32_t addr;
		const uintptr_t reminder = reinterpret_cast<uintptr_t>(values) % sizeof(uint32_t);
		if (reminder != 0)
		{
			const uintptr_t complement = sizeof(uint32_t) - reminder;

			addr = 0; // not strictly needed, but to quell any "used w/o initialization" warnings
			for (unsigned i = 0; i < complement; i++)
				addr = (addr << 8) | (*values++);

			addr <<= (4 - complement) * 8;

			t.addr = &addr;
			t.bits.addr = 8 * complement;
		}

		// FIXME: What if all data was put into addr and there is no mosi? Is that OK?
		t.mosi = reinterpret_cast<uint32_t*>(const_cast<uint8_t*>(values));
		t.bits.mosi = 8 * count;

		spi_trans(SPI_BUS, &t);
	}

	inline uint8_t readRegister(uint8_t r)
	{
		uint16_t cmd = r;
		uint32_t dataIn;

		spi_trans_t t;
		t.cmd = &cmd;
		t.miso = &dataIn;
		t.bits.val = 0;
		t.bits.cmd = 8;
		t.bits.miso = 8;

		spi_trans(SPI_BUS, &t);

		return static_cast<uint8_t>(dataIn);
	}

	enum class Mode : uint8_t
	{
		Sleep = 0b000,
		StandBy = 0b001,
		Transmit = 0b011
	};

	inline void setMode(Mode mode)
	{
		const uint8_t v = static_cast<uint8_t>(mode) << 2;
		writeRegister(RegOpMode, v);

		/*
		 * Wait for mode to be changed.
		 * As a mode change is generally quick and will take < 1ms, polling
		 * is the thing to do here.
		 */
		uint8_t flags = readRegister(RegIrqFlags1);
		while ((flags & BIT_u8(IRQ_FLAGS1_BIT_MODE_READY)) == 0)
			flags = readRegister(RegIrqFlags1);
	}

	inline void setBitRate(uint16_t bitrate)
	{
		const uint8_t values[2] =
		{
			static_cast<uint8_t>((bitrate >> 8) & 0xff), // MSB
			static_cast<uint8_t>(bitrate & 0xff) // LSB
		};
		writeRegisters(RegBitrateMsb, values, size(values));
	}

	inline void setFrequencyCoeff(uint32_t frequencyCoeff)
	{
		const uint8_t values[3] =
		{
			static_cast<uint8_t>((frequencyCoeff >> 16) & 0xff), // MSB
			static_cast<uint8_t>((frequencyCoeff >> 8) & 0xff), // Middle
			static_cast<uint8_t>(frequencyCoeff & 0xff) // LSB
		};
		writeRegisters(RegFrfMsb, values, size(values));
	}

	inline void setPreambleSize(uint16_t preambleSize)
	{
		const uint8_t values[2] =
		{
			static_cast<uint8_t>((preambleSize >> 8) & 0xff), // MSB
			static_cast<uint8_t>(preambleSize & 0xff) // LSB
		};
		writeRegisters(RegPreambleMsb, values, size(values));
	}

	/**
	 * Set transmit power level.
	 *
	 * For non-high power devices (RFM69W, RFM69CW), only the PA0 can be used
	 * and the valid range is -18 dBm to +13 dBm.
	 *
	 * For high-power devices (RFM69HW, RFM69HCW), PA0 cannot be used, but there
	 * are PA1 and PA2. The valid range is -2 dBm to +20 dBm.
	 *
	 * Based on work by André Hessling - https://github.com/ahessling/RFM69-STM32
	 *
	 * @param [in] level Desired power level in dBm.
	 * @returns True if the level was set, false otherwise (this means the level was out of the allowed range).
	 */
	bool setTxPower(int level);

	void initGPIO();
	void initSPI();
}

#endif // ESP8266_RTS_RFM69_H
