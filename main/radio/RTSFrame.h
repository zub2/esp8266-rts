/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_RTS_FRAME_H
#define ESP8266_RTS_RTS_FRAME_H

#include <cstdint>
#include <limits.h>

enum class Action: uint8_t
{
	my = 0x1,
	up = 0x2,
	my_up = 0x3,
	down = 0x4,
	my_down = 0x5,
	up_down = 0x6,
	prog = 0x8,
	sun_flag = 0x9,
	flag = 0xa
};

// https://pushstack.wordpress.com/somfy-rts-protocol/
struct RTSFrame
{
	uint8_t key;
	Action ctrl;
	uint16_t rollingCode;
	uint32_t address;

	static constexpr uint8_t SOMFY_FRAME_PAYLOAD_BYTES = 7;

	void encode(uint8_t (&frameBytes)[SOMFY_FRAME_PAYLOAD_BYTES]) const
	{
		frameBytes[0] = key;
		frameBytes[1] = static_cast<uint8_t>(ctrl) << 4;
		frameBytes[2] = static_cast<uint8_t>(rollingCode >> CHAR_BIT);
		frameBytes[3] = static_cast<uint8_t>(rollingCode);
		frameBytes[4] = static_cast<uint8_t>(address >> 2*CHAR_BIT);
		frameBytes[5] = static_cast<uint8_t>(address >> 1*CHAR_BIT);
		frameBytes[6] = static_cast<uint8_t>(address >> 0*CHAR_BIT);

		frameBytes[1] |= checksum(frameBytes);
		obfuscate(frameBytes);
	}

private:
	uint8_t checksum(uint8_t (&frameBytes)[SOMFY_FRAME_PAYLOAD_BYTES]) const
	{
		uint8_t c = 0;
		for (unsigned i = 0; i < SOMFY_FRAME_PAYLOAD_BYTES; i++)
		{
			uint8_t d = frameBytes[i];
			c = c ^ d ^ (d >> 4);
		}

		return c & 0xf;
	}

	void obfuscate(uint8_t (&frameBytes)[SOMFY_FRAME_PAYLOAD_BYTES]) const
	{
		for (size_t i = 1; i < SOMFY_FRAME_PAYLOAD_BYTES; i++)
			frameBytes[i] ^= frameBytes[i-1];
	}
};

#endif // ESP8266_RTS_RTS_FRAME_H
