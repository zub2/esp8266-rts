/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstdio>

#include "driver/gpio.h"

#include "NVS.h"
#include "serial/SerialTask.h"
#include "radio/RadioTask.h"
#include "wifi/Network.h"

extern "C" void app_main()
{
	serial::init();
	puts("esp8266-rtos-rfm69");

	nvs::init();

	gpio_install_isr_service(0);

	wifi::init();

	serial::start(1);
	radio::start(3);
	wifi::start(2);
}
