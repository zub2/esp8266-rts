/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Network.h"

#include "freertos/FreeRTOS.h"
#include "freertos/event_groups.h"
#include "freertos/semphr.h"

#include <cstring>
#include <unistd.h>

#include "esp_log.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_wifi_types.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"

#include "../NVS.h"
#include "../Utils.h"

#include "NetworkImpl.h"
#include "SimpleProto.h"

namespace wifi
{
	EventGroupHandle_t wifiEventGroup;
}

namespace
{
	const char LOG_TAG[] = "Network";

	SemaphoreHandle_t configMutex;

	const char NVS_KEY_SSID[] = "ssid";
	const char NVS_KEY_PASSWORD[] = "password";
	const char NVS_KEY_HOSTNAME_TEMPLATE[] = "hostnameTmpl";

	char ssid[wifi::MAX_SSID_LEN + 1] = CONFIG_DEFAULT_SSID;
	char password[wifi::MAX_PASSWORD_LEN + 1] = CONFIG_DEFAULT_PASSWORD;
	char hostnameTemplate[wifi::MAX_HOSTNAME_LEN + 1] = CONFIG_DEFAULT_HOSTNAME_TEMPLATE;

	void readWiFiConfig()
	{
		nvs::getStr(NVS_KEY_SSID, ssid);
		nvs::getStr(NVS_KEY_PASSWORD, password);
		nvs::getStr(NVS_KEY_HOSTNAME_TEMPLATE, hostnameTemplate);

		ESP_LOGD(LOG_TAG, "%s: ssid='%s', hostnameTemplate='%s'", __func__, ssid, hostnameTemplate);
	}

	// this function must be called with configMutex held
	void makeHostname(char (&hostname)[wifi::MAX_HOSTNAME_LEN + 1])
	{
		uint8_t mac[wifi::MAC_ADDR_LEN];
		ESP_ERROR_CHECK(esp_wifi_get_mac(ESP_IF_WIFI_STA, mac));
		snprintf(hostname, size(hostname)-1, hostnameTemplate, mac[3], mac[4], mac[5]);
		hostname[size(hostname)-1] = 0;
	}

	void setHostname()
	{
		MutexGuard g(configMutex);

		char hostname[wifi::MAX_HOSTNAME_LEN + 1];
		makeHostname(hostname);

		ESP_LOGI(LOG_TAG, "%s: using hostname '%s'", __func__, hostname);

		// if setting the hostname fails (because the user set it to something
		// the SDK does not like), don't go into a bootloop...
		ESP_ERROR_CHECK_WITHOUT_ABORT(tcpip_adapter_set_hostname(TCPIP_ADAPTER_IF_STA, hostname));
	}

	// this function must be called with configMutex held
	void setWiFiStaConfig(wifi_sta_config_t & staCfg)
	{
		strncpy(reinterpret_cast<char*>(staCfg.ssid), ssid, size(staCfg.ssid));
		strncpy(reinterpret_cast<char*>(staCfg.password), password, size(staCfg.password));
	}

	void applyWiFiConfig()
	{
		wifi_config_t wifiConfig = {};
		{
			MutexGuard g(configMutex);
			setWiFiStaConfig(wifiConfig.sta);
		}

		// if the config is changed, the SDK disconnects and reconnects,
		// otherwise it stays connected
		ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifiConfig));
	}

	extern "C" esp_err_t event_handler(void *, system_event_t *event)
	{
		switch(event->event_id)
		{
		case SYSTEM_EVENT_STA_START:
			// https://esp32.com/viewtopic.php?t=1406
			setHostname();
			esp_wifi_connect();
			break;

		case SYSTEM_EVENT_STA_GOT_IP:
			ESP_LOGI(LOG_TAG, "%s: got IPv4 address: %s", __func__, ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
			xEventGroupSetBits(wifi::wifiEventGroup, wifi::CONNECTED_BIT);
			break;

		case SYSTEM_EVENT_GOT_IP6:
			// this is probably the link-local v6 address... if it works at all
			ESP_LOGI(LOG_TAG, "%s: got IPv6 address: %s", __func__, ip6addr_ntoa(&event->event_info.got_ip6.ip6_info.ip));
			break;

		case SYSTEM_EVENT_STA_DISCONNECTED:
			{
				xEventGroupClearBits(wifi::wifiEventGroup, wifi::CONNECTED_BIT);

				system_event_info_t *info = &event->event_info;

				const char *strReason = nullptr;
				switch (info->disconnected.reason)
				{
				case WIFI_REASON_BEACON_TIMEOUT:
					strReason = "beacon timeout";
					break;

				case WIFI_REASON_NO_AP_FOUND:
					strReason = "AP not found";
					break;

				case WIFI_REASON_AUTH_FAIL:
					strReason = "authentication failed";
					break;

				case WIFI_REASON_ASSOC_FAIL:
					strReason = "association failed";
					break;

				case WIFI_REASON_HANDSHAKE_TIMEOUT:
					strReason = "handshake timed out";
					break;

				case WIFI_REASON_BASIC_RATE_NOT_SUPPORT:
					strReason = "basic rate not supported, will retry with in 82.11 bgn mode";

					// Switch to 802.11 bgn mode
					esp_wifi_set_protocol(ESP_IF_WIFI_STA, WIFI_PROTOCAL_11B | WIFI_PROTOCAL_11G | WIFI_PROTOCAL_11N);
					break;
				}

				if (strReason)
					ESP_LOGI(LOG_TAG, "%s: disconnect reason: %s", __func__, strReason);
				else
					ESP_LOGI(LOG_TAG, "%s: disconnect reason: %d", __func__, info->disconnected.reason);

				// keep trying...
				esp_wifi_connect();
			}
			break;

		default:
			// don't care
			break;
		}

		return ESP_OK;
	}
}

namespace wifi
{
	void init()
	{
		configMutex = xSemaphoreCreateMutex();

		wifiEventGroup = xEventGroupCreate();
		tcpip_adapter_init();
		ESP_ERROR_CHECK(esp_event_loop_init(event_handler, nullptr));

		wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
		ESP_ERROR_CHECK(esp_wifi_init(&cfg));

		ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

		// read initial config
		readWiFiConfig();
		applyWiFiConfig();

		ESP_ERROR_CHECK(esp_wifi_start());

		ESP_LOGI(LOG_TAG, "%s: connecting to ap SSID '%s'", __func__, ssid);

		initSimpleProto();
	}

	void start(UBaseType_t priority)
	{
		startSimpleProto(priority);
	}

	void getStatus(WiFiStatus & status)
	{
		MutexGuard g(configMutex);

		copyToCharArray(status.hostnameTemplate, hostnameTemplate);
		makeHostname(status.hostname);

		wifi_sta_config_t wifiStaConfig;
		esp_wifi_get_config(ESP_IF_WIFI_STA, reinterpret_cast<wifi_config_t*>(&wifiStaConfig));

		convertUint8ToCharArray(status.ssid, wifiStaConfig.ssid);
		convertUint8ToCharArray(status.password, wifiStaConfig.password);

		status.connected = isConnected();
		if (status.connected)
		{
			// bssid_set is false, but bssid is available. cool
			copyArray(status.bssid, wifiStaConfig.bssid);
			status.channel = wifiStaConfig.channel;

			tcpip_adapter_get_ip_info(TCPIP_ADAPTER_IF_STA, &status.ipv4Config);

			// try v6
			tcpip_adapter_get_ip6_linklocal(TCPIP_ADAPTER_IF_STA, &status.ipv6LinkLocalAddr);
		}
		else
		{
			copyToCharArray(status.ssid, ssid);
			copyToCharArray(status.password, password);
		}
	}

	void setHostnameTemplate(const char * newHostnameTemplate)
	{
		{
			MutexGuard g(configMutex);

			copyToCharArray(hostnameTemplate, newHostnameTemplate);
			nvs::setStr(NVS_KEY_HOSTNAME_TEMPLATE, hostnameTemplate);
		}

		// force DHCP release, change hostname and lease again
		tcpip_adapter_dhcpc_stop(TCPIP_ADAPTER_IF_STA);
		setHostname();
		tcpip_adapter_dhcpc_start(TCPIP_ADAPTER_IF_STA);
	}

	void setWiFiConfig(const char * newSsid, const char * newPassword)
	{
		{
			MutexGuard g(configMutex);

			copyToCharArray(ssid, newSsid);
			nvs::setStr(NVS_KEY_SSID, ssid);

			copyToCharArray(password, newPassword);
			nvs::setStr(NVS_KEY_PASSWORD, password);
		}

		applyWiFiConfig();
	}

	bool isConnected()
	{
		return (xEventGroupGetBits(wifiEventGroup) & CONNECTED_BIT) != 0;
	}
}
