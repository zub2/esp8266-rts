/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_NETWORK_H
#define ESP8266_RTS_NETWORK_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <cstdint>

#include "lwip/ip4_addr.h"
#include "lwip/ip6_addr.h"

#include "esp_wifi_types.h"
#include "tcpip_adapter.h"

#include "../radio/RTSFrame.h"

namespace wifi
{
	constexpr size_t MAX_SSID_LEN = 32;
	constexpr size_t MAX_PASSWORD_LEN = 64;
	constexpr size_t MAX_HOSTNAME_LEN = TCPIP_HOSTNAME_MAX_SIZE;
	constexpr size_t MAC_ADDR_LEN = 6;

	struct WiFiStatus
	{
		char hostnameTemplate[MAX_HOSTNAME_LEN + 1];
		char hostname[MAX_HOSTNAME_LEN + 1];

		char ssid[MAX_SSID_LEN + 1];
		char password[MAX_PASSWORD_LEN + 1];
		bool connected;
		uint8_t bssid[MAC_ADDR_LEN];
		uint8_t channel;
		tcpip_adapter_ip_info_t ipv4Config;
		ip6_addr_t ipv6LinkLocalAddr;
	};

	void init();
	void start(UBaseType_t priority);

	void getStatus(WiFiStatus & status);

	void setHostnameTemplate(const char * newHostnameTemplate);
	void setWiFiConfig(const char * newSsid, const char * newPassword);
}

#endif // ESP8266_RTS_NETWORK_H
