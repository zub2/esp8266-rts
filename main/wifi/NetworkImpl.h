/*
 * Copyright 2020 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_NETWORKIMPL_H
#define ESP8266_RTS_NETWORKIMPL_H

#include <cstdint>

#include "freertos/event_groups.h"

#include "../Utils.h"

namespace wifi
{
	extern EventGroupHandle_t wifiEventGroup;
	constexpr uint32_t CONNECTED_BIT = BIT_u32(0);

	bool isConnected();
}

#endif // ESP8266_RTS_NETWORKIMPL_H
