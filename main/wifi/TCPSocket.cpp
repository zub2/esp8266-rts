/*
 * Copyright 2020 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "TCPSocket.h"

namespace wifi
{
	// can't be "read" because ESP8266_RTOS_SDK defines a macro #read... ouch :-/
	bool TCPSocket::doRead(uint8_t * buffer, size_t size)
	{
		while (size > 0)
		{
			const ssize_t r = read(m_socket, buffer, size);
			if (r < 0)
			{
				ESP_LOGE("TCPSocket", "%s: read on a socket failed", __func__);
				size = 0;
				return false;
			}
			if (r == 0)
				return false; // socket closed
			else
				size -= static_cast<size_t>(r);
		}

		return true;
	}

	bool TCPSocket::doWrite(const uint8_t * buf, size_t count)
	{
		size_t written = 0;
		while (written < count)
		{
			const ssize_t r = write(m_socket, buf, count);
			if (r > 0)
			{
				written += static_cast<size_t>(r);
				buf += r;
			}
			else if (r < 0)
			{
				ESP_LOGE("TCPSocket", "%s: can't write to socket", __func__);
				return false;
			}
		}

		return true;
	}

	TCPListeningSocket::TCPListeningSocket(uint16_t port)
	{
		m_socket = socket(PF_INET, SOCK_STREAM, 0);
		if (m_socket < 0)
			fatalError("can't create socket");

		sockaddr_in addr = {};
		addr.sin_family = AF_INET;
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons(port);

		if (bind(m_socket, reinterpret_cast<sockaddr*>(&addr), sizeof(addr)) < 0)
			fatalError("can't bind socket");

		if (listen(m_socket, 0) < 0)
			fatalError("can't listen on socket");
	}

	// can't be named accept because lwIP defines accept as a macro :-(
	TCPSocket TCPListeningSocket::acceptConnection()
	{
		sockaddr_in addr;
		socklen_t addrSize = sizeof(addr);
		int s = accept(m_socket, reinterpret_cast<sockaddr*>(&addr), &addrSize);
		if (s < 0)
			fatalError("can't accept connection");

		ESP_LOGI("TCPSocket", "got connection from %s", inet_ntoa(addr.sin_addr));

		return TCPSocket(s);
	}
}
