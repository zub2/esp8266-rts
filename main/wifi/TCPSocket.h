/*
 * Copyright 2020 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ESP8266_RTS_TCPSOCKET_H
#define ESP8266_RTS_TCPSOCKET_H

#include <cstdint>

#include "esp_log.h"

#include "lwip/sockets.h"
#include "lwip/sys.h"

#include "../Utils.h"

namespace wifi
{
	class TCPSocket
	{
	public:
		explicit TCPSocket(int socket):
			m_socket(socket)
		{}

		// can't be "read" because ESP8266_RTOS_SDK defines a macro #read... ouch :-/
		bool doRead(uint8_t * buffer, size_t size);

		template<typename T>
		bool doRead(T & buffer)
		{
			return doRead(reinterpret_cast<uint8_t*>(&buffer), sizeof(buffer));
		}

		template<typename T, size_t N>
		bool doRead(T (&buffer)[N])
		{
			return doRead(reinterpret_cast<uint8_t*>(buffer), size(buffer));
		}

		bool doWrite(const uint8_t * buf, size_t count);

		template<typename T>
		bool doWrite(const T & buffer)
		{
			return doWrite(reinterpret_cast<const uint8_t *>(&buffer), sizeof(buffer));
		}

		template<typename T, size_t N>
		bool doWrite(T const (&buffer)[N])
		{
			return doWrite(reinterpret_cast<const uint8_t *>(buffer), size(buffer));
		}

		~TCPSocket()
		{
			close(m_socket);
		}

	private:
		int m_socket;
	};

	class TCPListeningSocket
	{
	public:
		TCPListeningSocket(uint16_t port);

		// can't be named accept because lwIP defines accept as a macro :-(
		TCPSocket acceptConnection();

		~TCPListeningSocket()
		{
			close(m_socket);
		}

	private:
		int m_socket;
	};
}

#endif // ESP8266_RTS_TCPSOCKET_H
