/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <boost/test/unit_test.hpp>

#include <cstdint>

#include "../main/radio/BuffersReader.h"

namespace
{
	uint8_t buffer0[] =
	{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9
	};

	uint8_t buffer1[] =
	{
		10, 11, 12, 13, 14, 15, 16, 17, 18, 19
	};

	uint8_t buffer2[] =
	{
		20, 21, 22, 23, 24, 25, 26, 27, 28, 29
	};

	const uint8_t * buffers[3] =
	{
		buffer0,
		buffer1,
		buffer2
	};

	const size_t sizes[3] =
	{
		std::size(buffer0),
		std::size(buffer1),
		std::size(buffer2)
	};

	const uint8_t * buffersWithHole1[5] =
	{
		nullptr,
		buffer0,
		nullptr,
		nullptr,
		buffer1
	};

	const size_t sizesWithHole1[5] =
	{
		0,
		std::size(buffer0),
		0,
		0,
		std::size(buffer1)
	};

	const uint8_t * buffersWithHole2[3] =
	{
		nullptr,
		nullptr,
		nullptr
	};

	const size_t sizesWithHole2[3] =
	{
		0,
		0,
		0
	};
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_nullAtEnd)
{
	BuffersReader reader(nullptr, nullptr, 0);
	BOOST_TEST(reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_nullGetChunk0)
{
	BuffersReader reader(nullptr, nullptr, 0);
	const uint8_t * buffer = nullptr;
	size_t size = 0;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 0);
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_nullGetChunk10)
{
	BuffersReader reader(nullptr, nullptr, 0);
	const uint8_t * buffer = nullptr;
	size_t size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 0);
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_1bufferAtEnd)
{
	BuffersReader reader(buffers, sizes, 1);
	BOOST_TEST(!reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_1bufferGetChunk0)
{
	BuffersReader reader(buffers, sizes, 1);
	const uint8_t * buffer = nullptr;
	size_t size = 0;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 0);
	BOOST_TEST(!reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_1bufferGetChunk5_4)
{
	BuffersReader reader(buffers, sizes, 1);
	const uint8_t * buffer = nullptr;
	size_t size = 5;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 5);
	BOOST_TEST(buffer == buffers[0]);
	BOOST_TEST(!reader.isAtEnd());

	size = 4;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 4);
	BOOST_TEST(buffer == buffers[0] + 5);
	BOOST_TEST(!reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_1bufferGetChunk10)
{
	BuffersReader reader(buffers, sizes, 1);
	const uint8_t * buffer = nullptr;
	size_t size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[0]);
	BOOST_TEST(reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_1bufferGetChunk20)
{
	BuffersReader reader(buffers, sizes, 1);
	const uint8_t * buffer = nullptr;
	size_t size = 20;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[0]);
	BOOST_TEST(reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_3buffersGetChunkAll)
{
	BuffersReader reader(buffers, sizes, 3);
	const uint8_t * buffer = nullptr;

	// get 5 bytes
	size_t size = 5;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 5);
	BOOST_TEST(buffer == buffers[0]);
	BOOST_TEST(!reader.isAtEnd());

	// request 10 bytes, but there is only 5 more in the buffer
	size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 5);
	BOOST_TEST(buffer == buffers[0] + 5);
	BOOST_TEST(!reader.isAtEnd());

	// request 20 bytes, we should get 10 from the second buffer
	size = 20;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[1]);
	BOOST_TEST(!reader.isAtEnd());

	// request 20 bytes, we should get 10 from the third buffer
	size = 20;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[2]);
	BOOST_TEST(reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_buffersWithHole1GetChunkAll)
{
	BuffersReader reader(buffersWithHole1, sizesWithHole1, std::size(buffersWithHole1));
	const uint8_t * buffer = nullptr;

	// get 10 bytes from the real buffer0
	size_t size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[0]);
	BOOST_TEST(!reader.isAtEnd());

	// get 10 bytes from the real buffer1
	size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 10);
	BOOST_TEST(buffer == buffers[1]);
	BOOST_TEST(reader.isAtEnd());
}

BOOST_AUTO_TEST_CASE(TestBuffersReader_buffersWithHole2)
{
	BuffersReader reader(buffersWithHole2, sizesWithHole2, std::size(buffersWithHole2));
	const uint8_t * buffer = nullptr;

	// already after ctor we should be atEnd
	BOOST_TEST(reader.isAtEnd());

	// request 10 bytes anyway
	size_t size = 10;
	reader.getChunk(buffer, size);
	BOOST_TEST(size == 0);
	BOOST_TEST(buffer == nullptr);
	BOOST_TEST(reader.isAtEnd());
}
