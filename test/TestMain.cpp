/*
 * Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
 *
 * This file is part of esp8266-rts.
 *
 * esp8266-rts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * esp8266-rts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.
 */
#define BOOST_TEST_MODULE ESP8266 RTS Test
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
