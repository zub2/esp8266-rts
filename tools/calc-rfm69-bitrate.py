#!/usr/bin/env python3
#
# Search for a good RFM69 bitrate value to produce pulses of given lengths by
# oversampling.

# The periods I can see my RTS remote using are a bit different from what I see
# on https://pushstack.wordpress.com/somfy-rts-protocol/

# periods that are needed
# The times I measure on my remote are a bit different from what is described
# in the post linked above.
periods = [
	# wakeup
	10400e-6,
	7100e-6,

	# hw sync
	2550e-6,
	2470e-6,

	# sw sync
	4800e-6,

	# half-symbol duration
	645e-6
]

# minimal period that is still acceptable
# It should be so that the resulting encoded frame fits in 64 bytes. That
# is the most the SDK interface can transmit w/o any risk of gaps. (One
# could squeeze a few more bytes out of it. But meh.)
MIN_PERIOD = 300e-6

# RFM69 F_{OSC} frequency
FXOSC = 32e6 # 32 MHz

# convert from bitrate (bits/s) to bitrate coefficient as used by the RFM69
def bitrateToRFM69BitrateCoeff(bitrate):
	return round(FXOSC / bitrate)

# convert from bitrate coefficient as used by the RFM69 to actual bitrate (bits/s)
def rfm69BitrateCoeffToBitrate(br):
	return FXOSC / br

# start with bitrate matching the shortest period
bitrateCoeff = bitrateToRFM69BitrateCoeff(1 / min(periods))
MIN_COEFF = bitrateToRFM69BitrateCoeff(1 / MIN_PERIOD)

results = dict()
while (bitrateCoeff >= MIN_COEFF):
	period = 1 / rfm69BitrateCoeffToBitrate(bitrateCoeff)
	error = max([ abs(p - (round(p / period) * period)) / p for p in periods ])

	if error not in results:
		results[error] = set()
	results[error].add(bitrateCoeff)

	bitrateCoeff = bitrateCoeff - 1

N = 40
# print first N best results
keys = list(results.keys())
keys.sort()

print(f'Top {N} results:')
for i in range(min(N, len(keys))):
	e = keys[i]
	relativeError = round(100*e, 3)
	print(f'#{i+1}: {relativeError}%:', end='')

	coeffs = results[e]
	for coeff in coeffs:
		period = round(1e6 / rfm69BitrateCoeffToBitrate(coeff), 3)
		print(f' {period}µs({coeff})', end='')
	print()
