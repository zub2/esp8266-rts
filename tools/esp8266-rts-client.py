#!/usr/bin/env python3
#
# Copyright 2019 David Kozub <zub at linux.fjfi.cvut.cz>
#
# This file is part of esp8266-rts.
#
# esp8266-rts is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# esp8266-rts is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with esp8266-rts.  If not, see <http://www.gnu.org/licenses/>.

# A simple command line client for esp8266-rts.
import sys
import argparse
import socket
import struct

ESP8266_RTS_PORT = 9772

MAGIC = (ord('S') << 24) | (ord('T') << 16) | (ord('R') << 8) | ord('!')
VERSION = 1

COMMAND_PING = 0
COMMAND_TRANSMIT = 1
COMMAND_TRANSMIT_CONTINUOUS = 2
COMMAND_RESET_RADIO = 3
COMMAND_READ_REGISTER = 4
COMMAND_WRITE_REGISTER = 5

REPLY_OK = 'OK\n'

RTS_COMMANDS = {
	'my': 0x1,
	'up': 0x2,
	'my_up': 0x3,
	'down': 0x4,
	'my_down': 0x5,
	'up_down': 0x6,
	'prog': 0x8,
	'sun_flag': 0x9,
	'flag': 0xa
}

def formatHeader(command):
	return struct.pack('<IBB', MAGIC, VERSION, command)

def receiveReply(s):
	reply = b''
	while True:
		r = s.recv(1024)
		if len(r) == 0:
			break
		reply = reply + r

	return reply

def checkReplyStatus(reply):
	replyString = reply.decode('utf-8')
	if replyString != REPLY_OK:
		print('ERROR: ' + replyString.rstrip())
		return False
	return True

def handleStandardReply(s):
	reply = receiveReply(s)
	return checkReplyStatus(reply)

def ping(args):
	print('COMMAND ping')
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.connect((args.host, args.port))

		request = formatHeader(COMMAND_PING)
		s.sendall(request)

		reply = receiveReply(s)

		# decode reply
		print('PING reply: "' + reply.decode('utf-8') + '"')

def transmit(args):
	print('COMMAND transmit');

	# reverse lookup of matching command name
	ctrlStr = '<unknown>'
	for name, value in RTS_COMMANDS.items():
		if value == args.ctrl:
			ctrlStr = name

	print(('Requesting RTS frame with:\n'
		'\tkey: {} (0x{:02x})\n'
		'\tctrl: {} ({}, 0x{:x})\n'
		'\trolling code: {} (0x{:04x})\n'
		'\taddress: {} (0x{:06x})').format(
		args.key, args.key,
		ctrlStr, args.ctrl, args.ctrl,
		args.rolling_code, args.rolling_code,
		args.address, args.address
	))

	if args.continuous:
		print('using continuous mode')
		rts_command = COMMAND_TRANSMIT_CONTINUOUS
	else:
		print('using single frame mode')
		rts_command = COMMAND_TRANSMIT

	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.connect((args.host, args.port))

		request = formatHeader(rts_command) + struct.pack('<BBHI', args.key,
			args.ctrl, args.rolling_code, args.address)
		s.sendall(request)

		if args.continuous:
			input('Press enter to stop RTS broadcast.')

def reset(args):
	print('COMMAND reset')
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.connect((args.host, args.port))

		request = formatHeader(COMMAND_RESET_RADIO)
		s.sendall(request)

		if handleStandardReply(s):
			print('OK')

def readRegister(args):
	if not args.batch:
		print('COMMAND readRegister')

	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.connect((args.host, args.port))

		request = formatHeader(COMMAND_READ_REGISTER) + struct.pack('B', args.register)
		s.sendall(request)

		reply = receiveReply(s)

		# find newline
		idx = reply.find(b'\n')

		if idx == -1 or idx != len(reply) - 2:
			print("Unexpected reply: " + reply.hex())
			return

		# split to message and value
		message = reply[0:idx+1]
		if checkReplyStatus(message):
			value = reply[idx+1:idx+2]
			( v, ) = struct.unpack('B', value)
			if (args.batch):
				print(value)
			else:
				print('register #0x{:x} = 0x{:x}'.format(args.register, v))

def writeRegister(args):
	print('COMMAND writeRegister')
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.connect((args.host, args.port))

		request = formatHeader(COMMAND_WRITE_REGISTER) + struct.pack('BB', args.register, args.value)
		s.sendall(request)

		handleStandardReply(s)

def parseUInt(s, maxValue):
	i = int(s, 0)
	if i < 0 or i > maxValue:
		raise argparse.ArgumentTypeError('argument must be an integer in the range 0..0x{:x}'.format(maxValue))
	return i

def makeUIntParser(maxValue):
	return lambda x: parseUInt(x, maxValue)

def parseCtrl(s):
	try:
		# first try parsing as command name
		return RTS_COMMANDS[s]
	except KeyError:
		# try parsing as 4bit uint
		return parseUInt(s, 0xf)
	except ValueError:
		raise argparse.ArgumentTypeError('argument must be a RTS command name or an integer in the range 0..0xf')

parser = argparse.ArgumentParser()
parser.add_argument('host', help='Host to connect to (the IP addr or name of the esp8266 device.)')
parser.add_argument('--port', help=('Port to connect to. Defaults to ' + str(ESP8266_RTS_PORT) + '.'),
	type=int, default=ESP8266_RTS_PORT)

subparsers = parser.add_subparsers()

pingParser = subparsers.add_parser('ping', help='ping the ESP8266 using the esp8266-rts protocol')
pingParser.set_defaults(func=ping)

transmitParser = subparsers.add_parser('transmit', help='transmit one or more RTS frames')
transmitParser.add_argument('--key', type=makeUIntParser(0xff), required=True)
transmitParser.add_argument('--ctrl', type=parseCtrl, required=True)
transmitParser.add_argument('--rolling_code', type=makeUIntParser(0xffff), required=True)
transmitParser.add_argument('--address', type=makeUIntParser(0xffffff), required=True)
transmitParser.add_argument('--continuous', action='store_true', help='Keep broadcasting repeat frame until enter is pressed.')
transmitParser.set_defaults(func=transmit)

resetParser = subparsers.add_parser('reset', help='reset the RFM69 radio module')
resetParser.set_defaults(func=reset)

readRegParser = subparsers.add_parser('readRegister', help='read a RFM69 register')
readRegParser.add_argument('register', type=makeUIntParser(0xff), help='Register number.')
readRegParser.add_argument('--batch', action='store_true', help='Only print the value without any noise. For use in scripts.')
readRegParser.set_defaults(func=readRegister)

writeRegParser = subparsers.add_parser('writeRegister', help='write a RFM69 register')
writeRegParser.add_argument('register', type=makeUIntParser(0xff), help='Register number.')
writeRegParser.add_argument('value', type=makeUIntParser(0xff), help='Value to write to the register.')
writeRegParser.set_defaults(func=writeRegister)

args = parser.parse_args()
if 'func' in args:
	args.func(args)
else:
	print(f'Command missing. For help run {sys.argv[0]} -h')
