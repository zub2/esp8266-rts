#!/usr/bin/env python3
#
# Convert between bitrate, bit period and RFM69 bitrate coefficient.
import sys
import argparse

# RFM69 F_{OSC} frequency
FXOSC = 32e6 # 32 MHz

# convert from bitrate (bits/s) to bitrate coefficient as used by the RFM69
def bitrateToRFM69BitrateCoeff(bitrate):
	return round(FXOSC / bitrate)

# convert from bitrate coefficient as used by the RFM69 to actual bitrate (bits/s)
def rfm69BitrateCoeffToBitrate(br):
	return FXOSC / br

parser = argparse.ArgumentParser()
parser.add_argument('--bitrate', help='Convert from bitrate (in bits/s).', type=float)
parser.add_argument('--bitrateCoeff', help='Convert from RFM69 bitrate coefficient value (RegBitrate).', type=int)
parser.add_argument('--period', help='Convert from bit period (in seconds).', type=float)
args = parser.parse_args()

inputTypeCount = int(args.bitrate != None) + int(args.bitrateCoeff != None) + int(args.period != None)
if inputTypeCount > 1:
	print('Only one of --bitrate, --bitrateCoeff and --period must specified.')
	sys.exit(1)

if inputTypeCount == 0:
	print('One of --bitrate, --bitrateCoeff and --period must specified.')
	sys.exit(1)

if args.bitrate:
	bitrate = args.bitrate
	period = 1/bitrate
	bitrateCoeff = bitrateToRFM69BitrateCoeff(bitrate)

	print(f'Bitrate {bitrate} b/s:')
	print('\tRFM69 Bitrate coeff: {} (0x{:02x})'.format(bitrateCoeff, bitrateCoeff))
	print(f'\tPeriod: {period*1e6}µs')
elif args.bitrateCoeff:
	bitrateCoeff = args.bitrateCoeff
	bitrate = rfm69BitrateCoeffToBitrate(bitrateCoeff)
	period = 1/bitrate

	print('RFM69 Bitrate coeff: {} (0x{:02x})'.format(bitrateCoeff, bitrateCoeff))
	print(f'\tBitrate {round(bitrate, 3)} b/s:')
	print(f'\tPeriod: {round(period*1e6, 3)}µs')
else:
	period = args.period
	bitrate = 1/period
	bitrateCoeff = bitrateToRFM69BitrateCoeff(bitrate)

	print(f'Period: {round(period*1e6, 3)}µs')
	print(f'\tBitrate {round(bitrate, 3)} b/s:')
	print('\tRFM69 Bitrate coeff: {} (0x{:02x})'.format(bitrateCoeff, bitrateCoeff))
