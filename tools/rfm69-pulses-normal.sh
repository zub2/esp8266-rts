#!/bin/sh

# This is based on values measured using my RTS remote.
./rfm69-pulses-to-stream.py \
	10094 \
	left \
	10400e-6 7100e-6 \
	2470e-6 2550e-6 \
	2470e-6 2550e-6 \
	4800e-6 645e-6
